package assignment3;

public class EmployeeBO {
	
//	10% if annualIncome > 5L
//	15% if annualIncome > 10L
//	20% if annualIncome > 20L
	
	public void calIncomeTax(EmployeeVo employee) {
		int annualIncome = employee.getAnnualIncome();
		int incomeTax = 0;
		
	
		if(annualIncome > 2000000) 
			incomeTax = (int) ((annualIncome-1000000) * 0.2 + 1250000);
		else if (annualIncome > 1000000) 
			incomeTax = (int) ((annualIncome-500000) * 0.15 + 50000);
		else if (annualIncome > 500000) 
			incomeTax = (int) (annualIncome * 0.1);
		
		employee.setIncomeTax(incomeTax);		
	}
}