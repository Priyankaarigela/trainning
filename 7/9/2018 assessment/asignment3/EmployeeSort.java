package assignment3;
import java.util.Comparator;

public class EmployeeSort implements Comparator<EmployeeVo>{

	@Override
	public int compare(EmployeeVo o1, EmployeeVo o2) {
		return o1.getIncomeTax() < o2.getIncomeTax() ?	1 : -1;
	}

}
